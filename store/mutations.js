import initialState from './state'

export default {
  RESET_STORE: (state) => {
    Object.assign(state, initialState())
  },

  SET_AUTH_USER: (state, { authUser, username, friends }) => {
    state.authUser = {
      uid: authUser.uid,
      email: authUser.email,
      name: authUser.displayName,
      username: username,
      friends: friends,
    }
  },
}
