export default {
  isLoggedIn: (state) => {
    try {
      return state.authUser.uid !== null
    } catch {
      return false
    }
  },
  getFriends: (state) => {
    try {
      return state.authUser.friends
    } catch {
      return false
    }
  },
}
