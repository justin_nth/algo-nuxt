export default {
  async nuxtServerInit({ dispatch }, ctx) {
    /** Get the VERIFIED authUser on the server */
    if (ctx.res && ctx.res.locals && ctx.res.locals.user) {
      const { allClaims: claims, ...authUser } = ctx.res.locals.user

      console.info(
        'Auth User verified on server-side. User: ',
        authUser,
        'Claims:',
        claims
      )

      await dispatch('onAuthStateChanged', {
        authUser,
        username,
        friends,
      })
    }
  },

  async onAuthStateChanged({ commit }, { authUser }) {
    try {
      if (!authUser) {
        commit('RESET_STORE')
        return
      }

      let username
      let friends = []
      if (authUser && authUser.getIdToken) {
        let uid = authUser.uid
        const userRef = this.$fire.firestore.collection('users').doc(uid)
        const userDoc = await userRef.get()
        username = userDoc.data().username
        for (let index = 0; index < userDoc.data().friends.length; index++) {
          let friend = userDoc.data().friends[index]
          friend = await friend.get()
          friend = friend.data()
          friends.push(friend)
        }
      }

      commit('SET_AUTH_USER', { authUser, username, friends })
    } catch (error) {
      console.error(error)
    }
  },
}
