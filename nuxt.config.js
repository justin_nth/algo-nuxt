const isDev = process.env.NODE_ENV === 'development'
const useEmulators = false // manually change if emulators needed

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Algo',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'initial-scale=1, viewport-fit=cover, width=device-width, maximum-scale=1, user-scalable=0',
      },
      {
        name: 'apple-mobile-web-app-capable',
        content: 'yes',
      },
      {
        name: 'apple-mobile-web-app-status-bar-style',
        content: 'black-translucent',
      },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  /*
   ** Global CSS
   */
  css: [{ src: './assets/scss/screen.scss', lang: 'scss' }],
  styleResources: {
    // your settings here
    sass: [],
    scss: ['@/assets/scss/_variables.scss'],
    less: [],
    stylus: [],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: '~/plugins/vue-touch', ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ['@nuxtjs/style-resources', '@nuxtjs/svg-sprite'],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // '@nuxtjs/onesignal',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: 'AIzaSyCi1meOTmIOCH42OJ8YZSk_VwdrSMxOYXg',
          authDomain: 'algo-f220e.firebaseapp.com',
          projectId: 'algo-f220e',
          storageBucket: 'algo-f220e.appspot.com',
          messagingSenderId: '738978297844',
          appId: '1:738978297844:web:88d8c984f356964f20aba1',
          measurementId: 'G-FKK2CL0WGC',
        },
        services: {
          auth: {
            initialize: {
              onAuthStateChangedAction: 'onAuthStateChanged',
            },
            ssr: true,
            emulatorPort: isDev && useEmulators ? 9099 : undefined,
            disableEmulatorWarnings: false,
          },
          firestore: {
            memoryOnly: false,
            enablePersistence: true,
            emulatorPort: isDev && useEmulators ? 8080 : undefined,
          },
        },
      },
    ],
  ],

  // oneSignal: {
  //   init: {
  //     appId: '94128fa9-9a97-4100-9c03-4278d793ed2f',
  //     allowLocalhostAsSecureOrigin: true,
  //     welcomeNotification: {
  //       disable: true,
  //     },
  //   },
  // },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      title: 'Algo',
      author: 'Justin North',
    },
    manifest: {
      name: "Algo, l'application d'apprentissage de l'algorithmique",
      short_name: 'Algo',
      lang: 'fr',
      display: 'standalone',
    },
    // workbox: {
    //   importScripts: ['/firebase-auth-sw.js'],
    //   // by default the workbox module will not install the service worker in dev environment to avoid conflicts with HMR
    //   // only set this true for testing and remember to always clear your browser cache in development
    //   dev: process.env.NODE_ENV === 'development',
    // },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
